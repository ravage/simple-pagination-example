<?php
define('ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);

require(ROOT . DS . 'Library/Autoloader.php');

spl_autoload_extensions ('.php');
spl_autoload_register(array(new Library\Autoloader(), 'register'));

use Library\Pager;

$sql = 'SELECT * FROM users';

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
  $pager = new Pager($sql, $_GET['page']);  
} else {
  $pager = new Pager($sql);
}
?>