<?php require('bootstrap.php') ?>

<!doctype hmtl>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pagination</title>
  </head>
  <body>
    <table>
      <thead>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Birthdate</th>
      </thead>
      <tbody>
        <?php foreach ($pager->getNextPage() as $page): ?>
        <tr>
          <td><?php echo $page->id ?></td>
          <td><?php echo $page->first_name ?></td>
          <td><?php echo $page->last_name ?></td>
          <td><?php echo $page->birthdate ?></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <p>
    <?php for ($i = 1; $i <= $pager->getPageCount(); $i++): ?>
      <?php if ($pager->getCurrentPageNumber() == $i): ?>
        <?php echo $i ?>
      <?php else: ?>
        <a href=?page=<?php echo $i ?>><?php echo $i ?></a>
      <?php endif ?>
    <?php endfor; ?>
    </p>
  </body>
</html>