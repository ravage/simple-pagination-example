<?php

namespace Library;

class Config {
  private static $cache = array();

  public static function get($key) {
    if (array_key_exists($key, static::$cache)) {
      return static::$cache[$key];
    }
    else {
      $tokens = explode('.', $key);
      static::load("config.$tokens[0]");

      if (array_key_exists($key, static::$cache)) {
        return static::$cache[$key];
      }
    }

    return false;
  }

  public static function load($file) {
    $prefixed = array();
    $tokens = explode('.', $file);
    $length = count($tokens);
    $path = ROOT . DS . implode(DS, $tokens);
    $path .= '.php';

    if (file_exists($path)) {
      $import = require($path);

      foreach ($import as $key => $value) {
        $prefixed["{$tokens[$length - 1]}.{$key}"] = $value;
      }
    }

    static::$cache = array_merge(static::$cache, $prefixed);

    return static::$cache;
  }
}

?>
