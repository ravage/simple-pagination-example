<?php 
namespace Library;

use Library\DB;

class Pager
{
  private $max;
  private $page;
  private $query;
  private $params;
  private $db;

  public function __construct($query, $page = 1, $max = 10)
  {
    $this->max = $max;
    $this->page = $page - 1;
    $this->query = $query;
    $this->db = DB::getInstance();
    $this->params = array();
  }

  public function setQueryParams($params = array()) {
    $this->params = $params;
  }

  public function getNextPage() {
    $this->page++;

    return $this->getPage();
  }

  public function getPreviousPage() {
    $this->page--;

    return $this->getPage();
  }

  public function getPageCount() {
    $pattern = '/^SELECT (.+?) FROM/i';
    $replace = 'SELECT COUNT(*) AS result FROM';

    $sql = preg_replace($pattern, $replace, $this->query);
    
    $stmt = $this->db->prepare($sql);
    $stmt->execute($this->params);

    return $stmt->fetch()->result / $this->max;
  }

  public function getCurrentPageNumber() {
    return $this->page;
  }

  private function getPage() {
    if ($this->page < 1) {
      $this->page = 1;
    } 

    $offset = ($this->page - 1) * $this->max;

    $sql = "$this->query LIMIT $this->max OFFSET $offset";

    $stmt = $this->db->prepare($sql);
    $stmt->execute($this->params);

    return $stmt->fetchAll();
  }
}
?>