<?php

namespace Library;

use Library;

class DB {
  private static $instance = null;

  private function __construct() {}

  public static function getInstance() {
    if (static::$instance == null) {
      $driver = Config::get('database.driver');
      $host = Config::get('database.host');
      $database = Config::get('database.name');
      $user = Config::get('database.username');
      $password = config::get('database.password');

      $dsn = "$driver:host=$host;dbname=$database;charset=utf8";

      static::$instance = new \PDO($dsn, $user, $password);

      static::$instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      static::$instance->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
    }

    return static::$instance;
  }
}

?>
