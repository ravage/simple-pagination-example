<?php

namespace Library;

class Autoloader {
  public function register($klass) {
    $tokens = explode('\\', $klass);

    array_unshift($tokens, ROOT);

    $path = implode(DS, $tokens) . '.php';

    if (file_exists($path)) {
      require_once($path);
    }
  }
}
